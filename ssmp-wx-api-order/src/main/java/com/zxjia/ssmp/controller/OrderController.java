package com.zxjia.ssmp.controller;

import com.zxjia.ssmp.dto.OrderRequest;
import com.zxjia.ssmp.service.OrderService;
import com.zxjia.ssmp.vo.OrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/order")
public class OrderController {

    @Autowired
    OrderService orderService;
    
    @PostMapping(value = "/createorder")
    public OrderVo createOrder(@RequestBody OrderRequest order) {
       return orderService.createOrder(order);
    }


}

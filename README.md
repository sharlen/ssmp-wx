## 项目介绍
#### ssmp-wx使用SpringCloud Alibaba+SpringBoot+MyBatis-Plus构建的一款基于微信公众号为载体运行的商城项目
#### 如果你需要一套后台管理系统用来查看管理，商品、用户、订单等信息你可以访问下面链接 
    | 后端下载地址  https://gitee.com/wuwangwoy/shop-admin     
    | 前端下载地址  https://gitee.com/wuwangwoy/shop-admin-web 


## 演示地址:
    微信打开扫一扫访问
![输入图片说明](https://images.gitee.com/uploads/images/2021/0310/141742_71493d10_376915.jpeg "qrcode_for_gh_5b50ebc715a7_258 (1).jpg")

## 如何启动:
- 第1步：首先需要安装maven环境下载依赖，配置镜像仓库，需安装jdk1.8以上环境。
- 第2步：（必须）安装nacos注册中心    https://nacos.io/zh-cn/index.html    1.x的版本最好
- 第3步：（非必须）安装sentinel服务监控 https://github.com/alibaba/sentinel    
- 第4步：查看application.yml文件修改对应的nacos地址，数据库连接等
- 第5步：执行ssmp-wx根目录下ssmp-wx.sql文件
- 第6步：按顺序启动
```
            ssmp-wx-api-user            用户管理模块
            ssmp-wx-api-product         商品购物车模块
            ssmp-wx-api-order           订单管理模块
            ssmp-wx-api                 前端管理模块
            ssmp-wx-gateway             服务网关
```
- 第7步：网关访问地址 http://localhost:9527/index.html
- 第8步：接口文档地址 http://localhost:9527/doc.html


## er设计图:

![输入图片说明](https://images.gitee.com/uploads/images/2021/0224/150220_10ae82ff_376915.png "屏幕截图.png")

## 接口文档:
![输入图片说明](https://images.gitee.com/uploads/images/2021/0210/173932_0c06f0fe_376915.png "屏幕截图.png")

## nacos截图:
![输入图片说明](https://images.gitee.com/uploads/images/2021/0219/111631_aa8f7d11_376915.png "屏幕截图.png")

## sentinel截图:
 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0224/112104_703c2313_376915.png "屏幕截图.png")


## 扩展功能:
    如果你想通过公众号的方式访问项目的话你可以申请一个测试账号
    https://mp.weixin.qq.com/debug/cgi-bin/sandbox?t=sandbox/login
申请了以后修改application.yml
```
wx:
  mp:
    useRedis: false
    redisConfig:
      host: 127.0.0.1
      port: 6379
    configs:
      - appId: 11     # 申请公众号的appid
        secret: 11    # 申请公众号的appsecret
        token: 111    # 接口配置里的Token值
        #aesKey: 111  # 接口配置里的EncodingAESKey值
```
###### 那么我没有服务器怎么办呢，本地联调很不方便，不要急可以选择一个内外映射工具网上有很多，我给大家推荐一个我在用的<br/>
    http://ngrok.ciqiuwl.cn


## QQ交流群
<a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=KBlFDIdTWlbYS3j5EWtyLEq6sjbATnN5&jump_from=webapi">~~335102947~~ </a>🈵️ 、<a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=AYY4TTfGFuotB6a9lzLlPdljQO_G9AZF&jump_from=webapi">1090283135</a> 🔥


## 技术框架:
| 名称     | 版本  | 链接                                               |
|--------|-----|--------------------------------------------------|
| SpringCloud | Hoxton.SR1 | https://spring.io/projects/spring-cloud |
| SpringCloud-Alibaba | 2.1.0.RELEASE | https://github.com/alibaba/spring-cloud-alibaba |
| SpringBoot | 2.2.3 | https://spring.io/projects/spring-boot |
| Nacos | 2.2.3 | https://nacos.io/zh-cn/index.html |
| Sentinel | 2.2.3 | https://github.com/alibaba/sentinel |
| Lombok | 1.18.10 | https://projectlombok.org |
| Mybatis-Plus | 3.4.1 | https://mp.baomidou.com |
| hutool | 5.4.2 | https://www.hutool.cn |
| knife4j | 2.0.8 | https://doc.xiaominfo.com |  


## 运行截图:
<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0220/164323_5a0fb923_376915.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0220/164401_c618be46_376915.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0220/164452_a20873a8_376915.png"/></td>
    </tr>
     <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0220/164518_f5bd6301_376915.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0220/164556_836afa05_376915.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0220/164619_e735bc81_376915.png"/></td>
    </tr>
</table>

package com.zxjia.ssmp.controller;

import com.zxjia.ssmp.dto.MemberAddressRequest;
import com.zxjia.ssmp.dto.MemberRequest;
import com.zxjia.ssmp.service.UserService;
import com.zxjia.ssmp.vo.MemberAddressVo;
import com.zxjia.ssmp.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "用户管理")
@Controller
@RequestMapping(value = "/api")
public class UserController {

    @Autowired
    UserService userService;

    @ApiOperation(value = "登录")
    @PostMapping("/user/login")
    @ResponseBody
    public ResultVO login(@RequestBody MemberRequest memberRequest) {
        return ResultVO.success(userService.login(memberRequest));
    }

    @ApiOperation(value = "注册")
    @PostMapping("/user/register")
    @ResponseBody
    public ResultVO register(@RequestBody MemberRequest memberRequest) {
        return ResultVO.success(userService.register(memberRequest));
    }

    @ApiOperation(value = "发送短信验证码")
    @PostMapping(value = "/user/sendSms")
    @ResponseBody
    public ResultVO sendSms(@RequestBody MemberRequest memberRequest) {
        return ResultVO.success(userService.sendSms(memberRequest));
    }


    @ApiOperation(value = "获取用户地址列表")
    @PostMapping(value = "/user/getMemberAddress")
    @ResponseBody
    public ResultVO<List<MemberAddressVo>> getMemberAddress(@RequestBody  MemberAddressRequest request) {
        return ResultVO.success(userService.getMemberAddress(request));
    }
}

package com.zxjia.ssmp.feign;


import com.zxjia.ssmp.dto.MemberAddressRequest;
import com.zxjia.ssmp.dto.MemberRequest;
import com.zxjia.ssmp.vo.MemberAddressVo;
import com.zxjia.ssmp.vo.MemberVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Component
@FeignClient(value = "ssmp-wx-api-user")
@RequestMapping("/api")
public interface UserApiService {

    @PostMapping(value = "/user/getmemberAddress")
    List<MemberAddressVo> getMemberAddress(@RequestBody MemberAddressRequest request);

    @PostMapping(value = "/user/getMemberByMobile")
    MemberVo getMemberByMobile(@RequestBody MemberRequest request);

    @PostMapping(value = "/user/addMember")
    boolean addMember(@RequestBody MemberRequest memberVo);

    @PostMapping(value = "/user/sendsms")
    MemberVo sendSms(@RequestBody MemberRequest memberRequest);
}

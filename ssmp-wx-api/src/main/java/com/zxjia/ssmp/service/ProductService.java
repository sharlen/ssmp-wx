package com.zxjia.ssmp.service;

import com.zxjia.ssmp.dto.CartRequest;
import com.zxjia.ssmp.vo.IndexVo;
import com.zxjia.ssmp.vo.ProductCateVo;
import com.zxjia.ssmp.vo.ProductImageVo;
import com.zxjia.ssmp.vo.ProductVo;
import java.util.List;

public interface ProductService {

    ProductVo getProductById(Integer productId);

    List<ProductCateVo> getProductCate();

    List<ProductVo> getProductByCateId(Integer catId);

    List<ProductImageVo> getProductImageById(Integer productId);

    boolean addCart( CartRequest request);

    IndexVo getProductCateById(Integer catId);
}


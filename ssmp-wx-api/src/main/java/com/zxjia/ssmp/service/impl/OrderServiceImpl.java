package com.zxjia.ssmp.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.zxjia.ssmp.dto.CartRequest;
import com.zxjia.ssmp.dto.OrderRequest;
import com.zxjia.ssmp.exception.BusinessException;
import com.zxjia.ssmp.feign.OrderApiService;
import com.zxjia.ssmp.service.CartService;
import com.zxjia.ssmp.service.OrderService;
import com.zxjia.ssmp.utils.ObjectUtil;
import com.zxjia.ssmp.vo.CartListVo;
import com.zxjia.ssmp.vo.CartVo;
import com.zxjia.ssmp.vo.OrderVo;
import com.zxjia.ssmp.vo.ProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private CartService cartService;
    @Autowired
    private OrderApiService orderApiService;

    @Override
    public OrderVo bill(OrderRequest orderRequest) {
        CartListVo cartListVo = cartService.getCart(ObjectUtil.initObject(new CartRequest())
                                .init(v -> v.setMemberId(orderRequest.getMemberId())).getObject());
        if(CollectionUtil.isEmpty(cartListVo.getCart())) {
            throw new BusinessException("请求数据失败");
        }
        List<ProductVo> productVos = cartListVo.getCart().stream().map(CartVo::getProductVo).collect(Collectors.toList());
        BigDecimal totalMoney = productVos.stream().map(ProductVo::getMallMobilePrice).reduce(BigDecimal.ZERO, BigDecimal::add);

        //目前没有做折扣这块 就都取商品金额
        OrderVo orderVo = new OrderVo();
        orderVo.setTotalMoney(totalMoney);
        return orderVo;
    }

    @Override
    public OrderVo createOrder(OrderRequest orderRequest) {
        return orderApiService.createOrder(orderRequest);
    }

}

package com.zxjia.ssmp.service;

import com.zxjia.ssmp.dto.OrderRequest;
import com.zxjia.ssmp.vo.OrderVo;

public interface OrderService {

    OrderVo bill(OrderRequest orderRequest);

    OrderVo createOrder(OrderRequest orderRequest);
}



/*分享弹层*/
$(".gdetails-hshare").click(function () {
 	 $(".gdetails-layer-bg").show();
 	 $(".gd-share-layer").show();
 	 $("body").addClass("gdetails-ovrerHide");
 	 $(".delivery-layer").hide();
});
$(".gd-share-layer-box1").click(function () {
 	 $(".gdetails-layer-bg").hide();
 	 $(".gd-share-layer").hide();
 	 $("body").removeClass("gdetails-ovrerHide");
});
$(".gdetails-layer-bg").click(function () {
 	 $(".gdetails-layer-bg").hide();
 	 $(".gd-share-layer").hide();
 	 $("body").removeClass("gdetails-ovrerHide");
});
/*加入购物车*/
$(function() {
     var addNumText = parseInt($(".add-num").text());
     if (addNumText==0) {
          $(".add-num").hide();
     }
     $(".addcar").click(function(event){
		  var token = localStorage.getItem("token");
		  if(token == null) {
			  // mlayer.login({
				//   btn: ['我再看看', '立即登陆'],
				//   yes: function(index) {
				// 	  alert('你选择了立即登陆');
				// 	  mlayer.close(index);
				//   }
			  // });
			  //return;
		  }
		 var member = localStorage.getItem("member");
		 if(member==null) {
			 mlayer.alert({
				 title: '消息',
				 content: '请先去登录',
				 btn: 'OK'
			 });
			 return;
		 }
		  var productId = $("#productId").val();
		  CoreUtil.sendPost("/api/cart/addCart",{"memberMobile":JSON.parse(member).mobile,"productId":productId,"number":1},function (res) {
			  if(res.code ==  200) {
				  $(".add-num").show();
				   addNumText++;
				  $(".add-num").text(addNumText);
			  } else {
				  mlayer.error({
					  title: res.msg,
					  time: 3
				  });
			  }
		  })

     });

     initBanner();

});

function initBanner() {
	var url=window.location.search; //获取url中"?"符后的字串
	var productId = url.substr(url.indexOf("=")+1);
	var index = mlayer.loading({
		title: '加载中'
	});
	CoreUtil.sendGet("/api/product/productDetails", {"productId":productId},function (res) {
		if(res.code ==  200) {
			var bd = "";
			for(var imvos of res.data.productImageVos) {
				bd+="<li><a href='javascript:void(0)'><img src='"+imvos.imgUrl+"' /></a></li>";
			}
			$(".banner ul").html(bd);
			TouchSlide({ slideCell:"#slideBox",autoPage:true,titCell:".hd ul",mainCell:".bd ul" });
			$("#description").attr("src",res.data.description)
			$("#productId").val(res.data.id);
			$("#mallMobilePrice").text(res.data.mallMobilePrice);
			$("#originalPrice").text(res.data.originalPrice);
			$("#productName").text(res.data.productName);
		} else {
			mlayer.error({
				title: res.msg,
				time: 3
			});
		}
		mlayer.close(index);
	});


}

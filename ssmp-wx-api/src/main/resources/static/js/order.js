function initMoney() {
    var member = localStorage.getItem("member");
    CoreUtil.sendPost("/api/order/bill", {"memberId": JSON.parse(member).id},function (res) {
        $("#totalMoney").html("￥" + res.data.totalMoney);
        $("#productMoney").html("￥" + res.data.totalMoney);
    });
}

$(function () {

    initProduct();
    initMoney();
})

function initProduct() {
    var member = localStorage.getItem("member");
    CoreUtil.sendPost("/api/cart/getCart", {"memberId": JSON.parse(member).id},function (res) {
        if(res.data.cart.length > 0) {
            var v = "";
            for (var cart of res.data.cart) {
             v+="<a href='javascript:;' class='aui-list-product-fl-item'> " +
                "<div class='aui-list-product-fl-img'> " +
                    "<img src='" + cart.productVo.imgUrl + "'> " +
                "</div>" +
                "<div class='aui-list-product-fl-text'> " +
                    "<h3 class='aui-list-product-fl-title'>" + cart.productVo.productName + "</h3>" +
                    "<div class='aui-list-product-fl-mes'>" +
                        "<div>" +
                            "<span class='aui-list-product-item-price'>" +
                                "<em>¥</em>" + cart.productVo.mallMobilePrice + "" +
                            "</span>" +

                        "</div>" +

                    "</div>" +
                    "<div class='aui-list-product-fl-bag'>" +
                        "<span><img src='themes/img/icon/bag1.png' alt=''></span>" +
                        "<span><img src='themes/img/icon/bag2.png' alt=''></span>" +
                    "</div>" +
                "</div>" +
            "</a>";
            }
            $(".aui-list-product-float-item").html(v)
        }
    });
}

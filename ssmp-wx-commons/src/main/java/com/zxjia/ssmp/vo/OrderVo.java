package com.zxjia.ssmp.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderVo {

    private BigDecimal totalMoney;
}

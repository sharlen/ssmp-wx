package com.zxjia.ssmp.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@TableName("cart")
@Data
public class Orders {

    private String orderSn;

    private String paySn;

    private String name;

    private String realName;

    private BigDecimal orderMoney;

    private String orderStatus;

    private String orderSouce;

    private Date createTime;
}

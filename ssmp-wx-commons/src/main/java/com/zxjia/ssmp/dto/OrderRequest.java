package com.zxjia.ssmp.dto;


import com.zxjia.ssmp.entity.Product;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class OrderRequest {


    private BigDecimal orderMoney;

    private BigDecimal discount;

    private Integer memberId;

    private List<Product> products;
}

package com.zxjia.ssmp.controller;

import com.zxjia.ssmp.dto.MemberAddressRequest;
import com.zxjia.ssmp.dto.MemberRequest;
import com.zxjia.ssmp.service.UserService;
import com.zxjia.ssmp.vo.MemberAddressVo;
import com.zxjia.ssmp.vo.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping(value = "/user/getmemberAddress")
    public List<MemberAddressVo> getMemberAddress(@RequestBody MemberAddressRequest request) {
        return userService.getMemberAddress(request);
    }

    @PostMapping(value = "/user/getMemberByMobile")
    public MemberVo getMemberByMobile(@RequestBody MemberRequest memberRequest) {
        return userService.getMemberByMobile(memberRequest);
    }

    @PostMapping(value = "/user/sendsms")
    public boolean sendSms(@RequestBody MemberRequest memberRequest) {
        return userService.sendSms(memberRequest);
    }

    @PostMapping(value = "/user/addMember")
    public boolean addMember(@RequestBody MemberRequest memberRequest) {
        return userService.addMember(memberRequest);
    }

}

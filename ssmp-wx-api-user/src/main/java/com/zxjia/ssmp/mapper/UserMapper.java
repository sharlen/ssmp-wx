package com.zxjia.ssmp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxjia.ssmp.entity.Member;
import com.zxjia.ssmp.entity.MemberAddress;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper  extends BaseMapper<MemberAddress> {

    @Select("select * from member where id=#{memberId}")
    Member getMmeberById(Integer memberId);


    @Select("select * from member where mobile=#{mobile}")
    Member getMmeberByMobile(String mobile);
}

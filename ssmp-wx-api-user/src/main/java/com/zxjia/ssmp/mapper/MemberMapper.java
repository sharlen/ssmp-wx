package com.zxjia.ssmp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxjia.ssmp.entity.Member;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberMapper extends BaseMapper<Member> {

}

package com.zxjia.ssmp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxjia.ssmp.entity.ProductCate;

public interface ProductCateMapper extends BaseMapper<ProductCate> {
}

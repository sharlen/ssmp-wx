package com.zxjia.ssmp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxjia.ssmp.entity.Product;

public interface ProductMapper extends BaseMapper<Product> {
}

package com.zxjia.ssmp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxjia.ssmp.entity.Cart;

public interface CartMapper extends BaseMapper<Cart> {
}

package com.zxjia.ssmp.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxjia.ssmp.entity.ProductImage;


public interface ProductImageMapper extends BaseMapper<ProductImage> {

 }
